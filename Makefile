NAME="example"
REPICENT=`gpg --list-keys | grep "^ " | tr -d " " | head -n 1`
index:
	ymp repo --index ./ --move --name="$(NAME)" --verbose --allow-oem --gpg:repicent=$(REPICENT)

git: index
	git add .
	git commit
	git push
