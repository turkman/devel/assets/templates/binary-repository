# Binary Repository Template
ymp binary package repository

## How to add package
1. copy package into repository directory
2. run **make** command
3. if you use **git** run **make git**

## How to add repository
1. run this command as root
```shell
# replace ymp-index.yaml with $uri
# always use '
ymp repo --add --name=example 'https://example.org/repo/$uri'
# Add gpg key
ymp key --add --name=example https://example.org/repo/ymp-index.yaml.asc
```
2. update index
```shell
ymp repo --update
```
